package inventory;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class PartTest {

    @BeforeAll
    static void setup(){
        System.out.println("@BeforeAll executed");
    }

    @BeforeEach
    void setupThis(){
        System.out.println("@BeforeEach executed");
    }

    @Tag("DEV")
    @Test
    void test1()
    {
        Part part = new InhousePart(1, "Piesa", 50, 200, 1, 300, 2);
        String error = "";
        error = Part.isValidPart(part.getName(), part.getPrice(), part.getInStock(), part.getMin(), part.getMax(), error);

        InventoryRepository inventoryRepository = new InventoryRepository();
        int size = inventoryRepository.getAllParts().size();

        Assertions.assertTrue(error.equals(""));

        inventoryRepository.addPart(part);
        Assertions.assertTrue(inventoryRepository.getAllParts().size()==size+1);
    }

    @Tag("DEV")
    @Test
    void test2()
    {
        Part part = new InhousePart(1, "", 50, 200, 1, 300, 2);
        String error = "";
        error = Part.isValidPart(part.getName(), part.getPrice(), part.getInStock(), part.getMin(), part.getMax(), error);

        Assertions.assertTrue(error.equals("A name has not been entered. "));
    }

    @Tag("DEV")
    @Test
    void test3()
    {
        Part part = new InhousePart(1, "Piesa", 0, 200, 1, 300, 2);
        String error = "";
        error = Part.isValidPart(part.getName(), part.getPrice(), part.getInStock(), part.getMin(), part.getMax(), error);

        Assertions.assertTrue(error.equals("The price must be greater than 0. "));
    }

    @Tag("DEV")
    @Test
    void test4()
    {
        Part part = new InhousePart(1, "", 0, 200, 1, 300, 2);
        String error = "";
        error = Part.isValidPart(part.getName(), part.getPrice(), part.getInStock(), part.getMin(), part.getMax(), error);

        Assertions.assertTrue(error.equals("A name has not been entered. The price must be greater than 0. "));
    }

    @Tag("DEV")
    @Test
    void test5()
    {
        Part part = new InhousePart(1, "Piesa", 50, 0, 1, 300, 2);
        String error = "";
        error = Part.isValidPart(part.getName(), part.getPrice(), part.getInStock(), part.getMin(), part.getMax(), error);

        Assertions.assertTrue(error.equals("Inventory level must be greater than 0. Inventory level is lower than minimum value. "));
    }

    @Tag("DEV")
    @Test
    void test6()
    {
        Part part = new InhousePart(1, "Piesa", 50, 350, 1, 300, 2);
        String error = "";
        error = Part.isValidPart(part.getName(), part.getPrice(), part.getInStock(), part.getMin(), part.getMax(), error);

        Assertions.assertTrue(error.equals("Inventory level is higher than the maximum value. "));
    }

    @Tag("DEV")
    @Test
    void test7()
    {
        Part part = new InhousePart(1, "Piesa", 50, 200, 1, 0, 2);
        String error = "";
        error = Part.isValidPart(part.getName(), part.getPrice(), part.getInStock(), part.getMin(), part.getMax(), error);

        Assertions.assertTrue(error.equals("The Min value must be less than the Max value. Inventory level is higher than the maximum value. "));
    }


    @AfterEach
    void tearThis(){

    }

    @AfterAll
    static void tear(){

    }
}
