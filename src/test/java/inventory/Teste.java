package inventory;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;

public class Teste {

    @BeforeAll
    static void setup() {
        System.out.println("@BeforeAll executed");
    }

    @BeforeEach
    void setupThis() {
        System.out.println("@BeforeEach executed");
    }

    @Tag("DEV")
    @Test
    void test1() {
        InventoryRepository inv = new InventoryRepository();
        Product product = inv.getProductFromString("P,1,name,2,3,4,5,6,1:2:3:4");
        Product expectedResult = new Product(1, "name", 2, 3, 4, 5, null);
        assert (product.getName().equals(expectedResult.getName())
                && product.getProductId() == expectedResult.getProductId());
    }

    @Tag("DEV")
    @Test
    void test2() {
        InventoryRepository inv = new InventoryRepository();
        Product product = inv.getProductFromString("P,9123,product2,12.34,12,1,12,4\n");
        Product expectedResult = new Product(9123, "product2", 12.34, 12, 1, 12, null);
        assert (product.getName().equals(expectedResult.getName())
                && product.getProductId() == expectedResult.getProductId());
    }

    @Tag("DEV")
    @Test
    void test3() {
        InventoryRepository inv = new InventoryRepository();
        Product product = inv.getProductFromString("P,1,name,2,3,4,5,6,1:2:3:4");
        Product expectedResult = new Product(1, "name", 2, 3, 4, 5, null);
        assert (product.getName().equals(expectedResult.getName())
                && product.getProductId() == expectedResult.getProductId());
    }

    @Tag("DEV")
    @Test
    void test4() {
        InventoryRepository inv = new InventoryRepository();
        Product product = inv.getProductFromString("I,1,name,2,3,4,5,6,1:2:3:4");
        assert product == null;
    }


    @AfterEach
    void tearThis() {

    }

    @AfterAll
    static void tear() {

    }
}
