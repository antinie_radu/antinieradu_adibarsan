package inventory.repository;

import inventory.model.Inventory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
class InventoryRepositoryTest {

    @Mock
    private Inventory inventory;

    @InjectMocks
    private InventoryRepository inventoryRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_getAutoProductId() {

        Mockito.when(inventory.getAutoProductId()).thenReturn(2);
        assert(inventoryRepository.getAutoProductId() == 2);
    }

    @Test
    public void test_getAutoPartId() {

        Mockito.when(inventory.getAutoPartId()).thenReturn(8);
        assert(inventoryRepository.getAutoPartId() == 8);
    }
}
