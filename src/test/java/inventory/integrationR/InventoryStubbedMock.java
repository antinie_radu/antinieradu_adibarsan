package inventory.integrationR;

import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.Product;

public class InventoryStubbedMock extends Inventory {

    public int getAutoPartId() {
        return 1;
    }

    public int getAutoProductId() {
        return 2;
    }

    public Part lookupPart(String any) {
        return new InhousePart(10, "part11", 1.23, 1, 1, 9, 123);
    }

    public Product lookupProduct(String any) {
        return new Product(2, "product2", 12.34, 12, 1, 12, null);
    }
}
