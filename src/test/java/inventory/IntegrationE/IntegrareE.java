package inventory.IntegrationE;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
class IntegrareE {

    private InventoryRepository inventoryRepository;

    private InventoryService inventoryService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        inventoryRepository = new InventoryRepository();
        inventoryService = new InventoryService(inventoryRepository);
    }

    @Test
    public void test_lookupPart() {

        Part expectedPart = new InhousePart(10, "part11", 1.23, 1, 1, 9, 123);
        String partString = "part11";

        assert(inventoryService.lookupPart(partString).getName().equals(expectedPart.getName()));
    }

    @Test
    public void test_lookupProduct() {

        Product expectedProduct = new Product(2, "product2", 12.34, 12, 1, 12, null);
        String productString = "product2";

        assert(inventoryRepository.lookupProduct(productString).getName().equals(productString));
    }
}
