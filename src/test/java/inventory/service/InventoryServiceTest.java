package inventory.service;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
class InventoryServiceTest {

    @Mock
    private InventoryRepository inventoryRepository;

    @InjectMocks
    private InventoryService inventoryService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_lookupPart() {

        Part expectedPart = new InhousePart(1, "1", 1, 1, 1, 1, 1);
        String partString = "1";
        Mockito.when(inventoryRepository.lookupPart(partString)).thenReturn(expectedPart);
        assert(inventoryRepository.lookupPart(partString) == expectedPart);
    }

    @Test
    public void test_lookupProduct() {

        Product expectedProduct = new Product(1, "1", 11, 1, 1, 1, null);
        String productString = "1";
        Mockito.when(inventoryRepository.lookupProduct(productString)).thenReturn(expectedProduct);
        assert(inventoryRepository.lookupProduct(productString) == expectedProduct);
    }

}
