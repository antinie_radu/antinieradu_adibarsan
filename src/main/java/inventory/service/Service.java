package inventory.service;

import inventory.model.Part;
import inventory.model.Product;
import javafx.collections.ObservableList;

public interface Service {
    void addInhousePart(String name, double price, int inStock, int min, int  max, int partDynamicValue);
    void addOutsourcePart(String name, double price, int inStock, int min, int  max, String partDynamicValue);
    void addProduct(String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts);
    ObservableList<Part> getAllParts();
    ObservableList<Product> getAllProducts();
}
