package inventory.repository;

import inventory.model.Product;

public interface Repository {
    void addProduct(Product product);
}
